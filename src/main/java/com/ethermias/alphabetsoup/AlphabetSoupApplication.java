package com.ethermias.alphabetsoup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlphabetSoupApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlphabetSoupApplication.class, args);
	}

}
