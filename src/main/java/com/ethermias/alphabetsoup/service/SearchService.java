package com.ethermias.alphabetsoup.service;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class SearchService {

    private int rows;
    private int cols;
    private char[][] grid;
    private List<String> wordsToFind;

    public List<String> search(InputStreamReader inputStreamReader) throws IOException {
        readInput(inputStreamReader);
        List<String> results = new ArrayList<>();
        for (String word : wordsToFind) {
            String result = findWord(word);
            if (result != null) {
                results.add(result);
            }
        }
        return results;
    }

    private void readInput(InputStreamReader inputStreamReader) throws IOException {
        try (BufferedReader reader = new BufferedReader(inputStreamReader)) {
            String[] dimensions = reader.readLine().trim().split("x");
            rows = Integer.parseInt(dimensions[0]);
            cols = Integer.parseInt(dimensions[1]);

            grid = new char[rows][cols];
            for (int i = 0; i < rows; i++) {
                String[] line = reader.readLine().trim().split(" ");
                for (int j = 0; j < cols; j++) {
                    grid[i][j] = line[j].charAt(0);
                }
            }

            wordsToFind = new ArrayList<>();
            String word;
            while ((word = reader.readLine()) != null) {
                wordsToFind.add(word.trim());
            }
        }
    }

    private String findWord(String word) {
        int[][] directions = {
            {0, 1},   
            {1, 0},   
            {0, -1},  
            {-1, 0}, 
            {1, 1}, 
            {-1, -1},
            {1, -1},
            {-1, 1} 
        };

        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                for (int[] dir : directions) {
                    String[] results = searchFrom(r, c, dir[0], dir[1], word);
                    if (results != null) {
                        return String.format("%s %s %s", word, results[0], results[1]);
                    }
                    // Try searching for the reversed word
                    String reversedWord = new StringBuilder(word).reverse().toString();
                    results = searchFrom(r, c, dir[0], dir[1], reversedWord);
                    if (results != null) {
                        return String.format("%s %s %s", word, results[1], results[0]);
                    }
                }
            }
        }
        return null;
    }

    private String[] searchFrom(int r, int c, int dr, int dc, String word) {
        int len = word.length();
        int endR = r + (len - 1) * dr;
        int endC = c + (len - 1) * dc;
        if (endR < 0 || endR >= rows || endC < 0 || endC >= cols) {
            return null;
        }

        for (int i = 0; i < len; i++) {
            if (grid[r + i * dr][c + i * dc] != word.charAt(i)) {
                return null;
            }
        }

        return new String[]{String.format("%d:%d", r, c), String.format("%d:%d", endR, endC)};
    }
}