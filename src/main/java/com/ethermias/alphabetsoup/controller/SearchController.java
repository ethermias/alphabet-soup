package com.ethermias.alphabetsoup.controller;
import com.ethermias.alphabetsoup.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

@RestController
@RequestMapping("/api")
public class SearchController {

    @Autowired
    private SearchService SearchService;


    @PostMapping("/search")
    public ResponseEntity<List<String>> search(@RequestParam("file") MultipartFile file) throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(file.getInputStream());
        List<String> results = SearchService.search(inputStreamReader);
        return ResponseEntity.ok(results);
    }
}
