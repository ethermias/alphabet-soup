package com.ethermias.alphabetsoup.controller;

import com.ethermias.alphabetsoup.service.SearchService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;

import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(SearchController.class)
public class SearchControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SearchService searchService;

    @Test
    public void testSearch() throws Exception {
        MockMultipartFile file = new MockMultipartFile(
                "file", 
                "test.txt", 
                MediaType.TEXT_PLAIN_VALUE, 
                "sample content".getBytes()
        );

        List<String> mockResult = Arrays.asList("result1", "result2");
        Mockito.when(searchService.search(any(InputStreamReader.class))).thenReturn(mockResult);

        mockMvc.perform(multipart("/api/search").file(file))
                .andExpect(status().isOk())
                .andExpect(content().json("[\"result1\",\"result2\"]"));
    }
}
