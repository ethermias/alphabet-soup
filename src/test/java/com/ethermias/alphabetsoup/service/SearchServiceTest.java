package com.ethermias.alphabetsoup.service;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class SearchServiceTest {

    private SearchService searchService;

    @BeforeEach
    public void setUp() {
        searchService = new SearchService();
    }

    @Test
    public void testSearch() throws IOException {
        // Mock input stream reader with a string
        String input = "3x3\nA B C\nD E F\nG H I\nABC\nDEF\nGHI\n";
        InputStreamReader inputStreamReader = new InputStreamReader(
                new java.io.ByteArrayInputStream(input.getBytes()));

        List<String> results = searchService.search(inputStreamReader);

        assertEquals(3, results.size());
        assertEquals("ABC 0:0 0:2", results.get(0));
        assertEquals("DEF 1:0 1:2", results.get(1));
        assertEquals("GHI 2:0 2:2", results.get(2));
    }

}
